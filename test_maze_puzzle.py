from __init__ import *

import unittest
import random

from maze_puzzle import *

class Test_Maze(unittest.TestCase):
  
  def setUp(self):
    self.maze = [((i,j),random.choice(['+','-'])) for i in xrange(10) for j in xrange(10)]
    self.exit_pos = random.choice([i for i,j in self.maze if j == '-'])
    self.cur_pos = self.start_pos = random.choice([i for i,j in self.maze if j == '-' and i != self.exit_pos])

  def test_start_exit_pos(self):
    '''Should never be Same. Return False'''
    self.assertNotEqual(self.exit_pos,self.start_pos)
  
  @unittest.SkipTest  
  def test_check_exit_pos(self):
    #Check if Position is Exit Coordindates
    self.assert_(check_exit_pos(self.exit_pos,self.cur_pos))
  
  def test_switch_directions(self):
    #Switch Directions
    pass


  
if __name__ == "__main__":
  unittest.main()