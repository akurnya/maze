#Maze Game Puzzle

'''
#initialize

#Check if Position is Exit Coordindates

#Switch Directions

#Check Next Move

#Check if it's Obstruction/Wall

# If yes Check if All 4 sides report Obstruction at Point P
#If Yes Raise NO WAY OUT Error
#If No Switch Direction and check next move

#If No, then Move
#Store Moved Trail in Store_trail
#Compare if the current Stored Position in Trail is actually Exit
#If yes, Hurray, exit return the Complete Path
#Else Repeat to Next Move
'''
import random

direction = {'N':0,'S':1,'W':2,'E':3}
dir_addr = ('N','S','W','E')
opposite = {'N':'S','E':'W','S':'N','W':'E'}
go_west = lambda pos: (pos[0] - 1,pos[1])
go_east = lambda pos: (pos[0] + 1,pos[1])
go_south = lambda pos: (pos[0], pos[1] - 1)
go_north = lambda pos: (pos[0], pos[1] + 1)
next_direct = lambda X: (X+1)%4
go_calls = {'N':go_north,'S':go_south,'W':go_west,'E':go_east}


def initialize(x=10,y=10):
    maze = [((i,j),random.choice(['+','-'])) for i in xrange(x) for j in xrange(y)]
    exit_pos = random.choice([i for i,j in maze if j == '-'])
    cur_pos = start_pos = random.choice([i for i,j in maze if j == '-' and i != exit_pos])
    cur_dirn = [0,0,0,0] #N,S,W,E
    cur_pos_dir = [cur_pos,[0,0,0,0]]
    return maze,start_pos,exit_pos,cur_pos,cur_dirn,cur_pos_dir

def check_exit_pos(exit_pos,cur_pos):
    #Check if Position is Exit Coordindates
    return exit_pos == cur_pos

#If next_move possible
def next_move(cur_pos_dir,cur_direction,maze_trail, maze):
    '''
    cur_pos_dir = [(9,8), [1,0,1,1]]
    cur_direction: one of N,S,W,E\
    maze: Given grid
    Check if cur_pos not already covered in Maze_Trail
    '''
    def match_pos_with_maze(pos,maze):
        '''If that position is Open in maze grid'''
        return (pos,'-') in maze
    pos,axis = cur_pos_dir
    new_pos = go_calls[cur_direction](pos)
    if match_pos_with_maze(new_pos,maze):
        return new_pos not in maze_trail
    return False

#Then move
def move(cur_pos_dir,cur_direction):
    pos,axis = cur_pos_dir
    new_pos = go_calls[cur_direction](pos)
    return [new_pos,[0,0,0,0]] #maze_trail.extend([new_pos,[0,0,0,0]])

#Else calculate switching dirn parameters
def cal_switch_params(cur_direction,cur_pos_dir):
    '''Calculates all switching direction parameters required for switch_dirn()
    w/o its checks'''
    pos,axis = cur_pos_dir
    cur_dir_number = direction[cur_direction]
    next_direction_num = next_direct(cur_dir_number)
    next_dir_addr = dir_addr[next_direction_num]
    return next_direction_num, next_dir_addr

#And switch direction
def switch_dirn(cur_pos_dir,next_direction_num,next_dir_addr):
    '''
    cur_direction: one of N,S,W,E
    if cur_pos_dir = [(9,8), [1,0,1,1]]
    if cur_direction = 'N' then
    return: 'S' and make cur_pos_dir = [(9,8), [1,1,1,1]]
    cur_direction = switch_dirn('N',  [(9,8), [1,0,1,1]]) = 'S' 
    '''
    pos,axis = cur_pos_dir
    if axis != [1,1,1,1]:
        #cur_dir_number = direction[cur_direction]
        #next_direction_num = next_direct(cur_dir_number)
        if axis[next_direction_num] == 0:  #axis[cur_dir_number]:
            axis[next_direction_num] += 1
            return dir_addr[next_direction_num]
    #raise Exception("NO WAY OUT!")
    return None


def step_back_with_next_move(cur_direction,maze_trail,maze, start_pos):
    '''
    If Switch direction is None then step back in maze_trail and 
    check for any openings with next_move()
    '''
    opp_dir = opposite[cur_direction]
    while maze_trail:
        l = maze_trail.pop()
        if start_pos == l[0]:
            raise Exception("NO WAY OUT!")
            break;
        cur_pos_dir = maze_trail[-1]
        pos,axis = cur_pos_dir
        for num,ax in enumerate(axis):
            if ax == 0:
                open_direction = dir_addr[num]
                if next_move(cur_pos_dir,open_direction,maze_trail, maze):
                    #[new_pos,[0,0,0,0]] #maze_trail.extend([new_pos,[0,0,0,0]])
                    #return Then maze_trail.extend
                    return move(cur_pos_dir,open_direction)
    else: raise Exception("NO WAY OUT!")
    
    

if __name__ == "__main__":
    #Procedural Flow of execution - An example
    maze_trail = []
    cur_direction = 'S'
    maze,start_pos,exit_pos,cur_pos,cur_dirn,cur_pos_dir = initialize()
    maze_trail.extend(cur_pos_dir)
    while not check_exit_pos(exit_pos,cur_pos):
        if cur_direction == None:
            raise Exception("NO WAY OUT!")
        if next_move(cur_pos_dir,cur_direction,maze_trail, maze):
            cur_pos_dir = move(cur_pos_dir,cur_direction) #[new_pos,[0,0,0,0]]
            maze_trail.extend(cur_pos_dir)
        else:
            next_direction_num, next_dir_addr = cal_switch_params(cur_direction,cur_pos_dir)
            cur_direction = switch_dirn(cur_pos_dir,next_direction_num,next_dir_addr)
        cur_pos = cur_pos_dir[0]
    else:
        print "start_pos {}".format(start_pos)
        print "cur_pos {}".format(cur_pos)
        print "exit_pos {}".format(exit_pos) 
        print maze_trail
        print "Out!"
        print maze